
referred from https://github.com/bushvin/zabbix-kvm-res
1. pip install libvirt-python
2. cp zabbix-kvm.py /etc/zabbix/
3. import zabbix kvm.xml into zabbix frontend.I work in zabbix 3.X,perhaps it will be some troble in importing to zabbix2.X frontend
4. 修改 /etc/zabbix_agentd.conf AllowRoot=1  (8.3  8.4的配置文件路径不一样)
增加
UserParameter=kvm.domain.discover,python3 /etc/zabbix/zabbix-kvm.py --item discovery
UserParameter=kvm.domain.cpu_util[*],python3 /etc/zabbix/zabbix-kvm.py --item cpu --uuid $1
UserParameter=kvm.domain.mem_usage[*],python3 /etc/zabbix/zabbix-kvm.py --item mem --uuid $1
UserParameter=kvm.domain.net_in[*],python3 /etc/zabbix/zabbix-kvm.py --item net_in --uuid $1
UserParameter=kvm.domain.net_out[*],python3 /etc/zabbix/zabbix-kvm.py --item net_out --uuid $1
UserParameter=kvm.domain.rd_bytes[*],python3 /etc/zabbix/zabbix-kvm.py --item rd_bytes --uuid $1
UserParameter=kvm.domain.wr_bytes[*],python3 /etc/zabbix/zabbix-kvm.py --item wr_bytes --uuid $1
systemctl restart zabbix-agent.service