controllerIpport=10.40.73.250:6082

 
#控制节点
yum install openstack-nova-spicehtml5proxy spice-vdagent.loongarch64 -y
wget https://www.spice-space.org/download/spice-html5/spice-html5-0.1.1-1.fc18.noarch.rpm
rpm -ivh spice-html5-0.1.1-1.fc18.noarch.rpm
crudini --set /etc/nova/nova.conf  vnc enabled false
crudini --set /etc/nova/nova.conf  spice html5proxy_host 0.0.0.0
crudini --set /etc/nova/nova.conf  spice html5proxy_port  6082
crudini --set /etc/nova/nova.conf  spice enabled True
crudini --set /etc/nova/nova.conf  spice html5proxy_base_url http://$controllerIpport/spice_auto.html
crudini --set /etc/nova/nova.conf  spice server_listen 0.0.0.0
crudini --set /etc/nova/nova.conf  spice agent_enabled true
crudini --set /etc/nova/nova.conf  spice keymap en-us

iptables -I INPUT -p tcp -m multiport --dports 6082 -m comment --comment "Allow SPICE connections for console access " -j ACCEPT
systemctl restart httpd openstack-nova-spicehtml5proxy spice-vdagentd.service
systemctl enable openstack-nova-spicehtml5proxy spice-vdagentd.service
  

#计算节点
wget https://www.spice-space.org/download/spice-html5/spice-html5-0.1.1-1.fc18.noarch.rpm
rpm -ivh spice-html5-0.1.1-1.fc18.noarch.rpm
crudini --set /etc/nova/nova.conf  vnc enabled false
crudini --set /etc/nova/nova.conf  spice enabled True
crudini --set /etc/nova/nova.conf  spice html5proxy_base_url http://$controllerIpport/spice_auto.html
crudini --set /etc/nova/nova.conf  spice agent_enabled true
crudini --set /etc/nova/nova.conf  spice keymap en-us
crudini --set /etc/nova/nova.conf  spice server_listen 0.0.0.0
systemctl restart openstack-nova-compute.service 

