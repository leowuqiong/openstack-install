#https://docs.jumpserver.org/zh/master/install/setup_by_fast/#_5
yum install -y wget curl tar gettext iptables
cd /opt
wget https://cdn0-download-offline-installer.fit2cloud.com/jumpserver/jumpserver-offline-installer-v3.8.2-loong64.tar.gz
tar -xzvf jumpserver-*
cd jumpserver-*


# 安装
./jmsctl.sh install

# 启动
./jmsctl.sh start


# 停止
./jmsctl.sh down

# 卸载
./jmsctl.sh uninstall

# 帮助
./jmsctl.sh -h

默认用户名密码：admin/admin