yum install -y tigervnc-server
yum install -y tigervnc-server-module
#设置密码：
vncpasswd

#修改配置文件：
#vim /root/.vnc/xstartup, 把原有的都注释掉了，增加了后面的内
//////////////////////////////////
#!/bin/sh
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic &
mate-session &
////////////////////////////////////

 #启动 
 vncserver

# 方法一：
# vncserver -list    #查看vnc的端口号

# TigerVNC server sessions:
# X DISPLAY #     PROCESS ID
# :1              8881

# vncserver -kill :1    #kill相关端口号

