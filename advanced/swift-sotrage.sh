
#每个存储节点执行
source openstack-rc
source variable-ha.sh
yum install xfsprogs rsync rsync-daemon
mkfs.xfs /dev/sdb1
mkfs.xfs /dev/sdc1

mkdir -p /srv/node/sdb
mkdir -p /srv/node/sdc

blkid

 /etc/fstab
UUID="" /srv/node/sdb xfs noatime,nodiratime,logbufs=8 0 2
UUID="" /srv/node/sdc xfs noatime,nodiratime,logbufs=8 0 2

mount /srv/node/sdb
mount /srv/node/sdc

cat >> /etc/rsyncd.conf <<eof
/etc/rsyncd.conf
uid = swift
gid = swift
log file = /var/log/rsyncd.log
pid file = /var/run/rsyncd.pid
address = MANAGEMENT_INTERFACE_IP_ADDRESS

[account]
max connections = 2
path = /srv/node/
read only = False
lock file = /var/lock/account.lock

[container]
max connections = 2
path = /srv/node/
read only = False
lock file = /var/lock/container.lock

[object]
max connections = 2
path = /srv/node/
read only = False
lock file = /var/lock/object.lock
eof



systemctl enable rsyncd.service
systemctl start rsyncd.service

yum install -y openstack-swift-account openstack-swift-container \
  openstack-swift-object

curl -o /etc/swift/account-server.conf https://opendev.org/openstack/swift/raw/branch/master/etc/account-server.conf-sample
curl -o /etc/swift/container-server.conf https://opendev.org/openstack/swift/raw/branch/master/etc/container-server.conf-sample
curl -o /etc/swift/object-server.conf https://opendev.org/openstack/swift/raw/branch/master/etc/object-server.conf-sample



cat > /etc/swift/account-server.conf <<eof
[DEFAULT]
bind_ip = $controller
bind_port = 6202
user = swift
devices = /srv/node
mount_check = true
[pipeline:main]
pipeline = healthcheck recon  account-server
[app:account-server]
use = egg:swift#account
[filter:healthcheck]
use = egg:swift#healthcheck
[filter:recon]
use = egg:swift#recon
recon_cache_path = /var/cache/swift
[filter:backend_ratelimit]
use = egg:swift#backend_ratelimit
[account-replicator]
[account-auditor]
[account-reaper]
[filter:xprofile]
use = egg:swift#xprofile
eof


cat > /etc/swift/container-server.conf <<eof
[DEFAULT]
bind_ip = $controller
bind_port = 6201
user = swift
swift_dir = /etc/swift
devices = /srv/node
mount_check = True
[pipeline:main]
pipeline = healthcheck recon  container-server
[app:container-server]
use = egg:swift#container
[filter:healthcheck]
use = egg:swift#healthcheck
[filter:recon]
use = egg:swift#recon
recon_cache_path = /var/cache/swift
[filter:backend_ratelimit]
use = egg:swift#backend_ratelimit
[container-replicator]
[container-updater]
[container-auditor]
[container-sync]
[filter:xprofile]
use = egg:swift#xprofile
[container-sharder]
eof


cat >/etc/swift/object-server.conf <<eof
 [DEFAULT]
bind_ip = $controller
bind_port = 6200
user = swift
swift_dir = /etc/swift
devices = /srv/node
mount_check = True
[pipeline:main]
pipeline = healthcheck recon  object-server
[app:object-server]
use = egg:swift#object
[filter:healthcheck]
use = egg:swift#healthcheck
[filter:recon]
use = egg:swift#recon
recon_cache_path = /var/cache/swift
recon_lock_path = /var/lock
[filter:backend_ratelimit]
use = egg:swift#backend_ratelimit
[object-replicator]
[object-reconstructor]
[object-updater]
[object-auditor]
[object-expirer]
[filter:xprofile]
use = egg:swift#xprofile
[object-relinker]
eof

cat >/etc/swift/swift.conf <<eof
[swift-hash]
swift_hash_path_suffix = HASH_PATH_SUFFIX
swift_hash_path_prefix = HASH_PATH_PREFIX
[storage-policy:0]
name = Policy-0
default = yes
eof

chown -R swift:swift /srv/node
mkdir -p /var/cache/swift
chown -R root:swift /var/cache/swift
chmod -R 775 /var/cache/swift

cd /etc/swift

# sudo swift-ring-builder account.builder remove -u 0
# sudo swift-ring-builder account.builder remove -u 1
# sudo swift-ring-builder account.builder remove -u 2

# sudo swift-ring-builder container.builder remove -u 0
# sudo swift-ring-builder container.builder remove -u 1
# sudo swift-ring-builder container.builder remove -u 2

# sudo swift-ring-builder object.builder remove -u 0
# sudo swift-ring-builder object.builder remove -u 1
# sudo swift-ring-builder object.builder remove -u 2

# rm -f *.gz

#下面操作需要在控制节点执行，然后将 account.ring.gz container.ring.gz object.ring.gz分发到每个存储节点
sudo swift-ring-builder account.builder create 10 1 1
sudo swift-ring-builder account.builder add --region 1 --zone 1 --ip 10.40.65.244 --port 6202 --device sdb --weight 100
sudo swift-ring-builder account.builder add --region 1 --zone 2 --ip 10.40.65.243 --port 6202 --device sdc --weight 100
sudo swift-ring-builder account.builder add --region 1 --zone 3 --ip 10.40.65.242 --port 6202 --device sdd --weight 100
swift-ring-builder account.builder rebalance



sudo swift-ring-builder container.builder create 10 1 1
sudo swift-ring-builder container.builder add --region 1 --zone 1 --ip 10.40.65.244 --port 6201 --device sdb --weight 100
sudo swift-ring-builder container.builder add --region 1 --zone 2 --ip 10.40.65.243 --port 6201 --device sdc --weight 100
sudo swift-ring-builder container.builder add --region 1 --zone 3 --ip 10.40.65.242 --port 6201 --device sdd --weight 100
swift-ring-builder container.builder rebalance


sudo swift-ring-builder object.builder create 10 1 1
sudo swift-ring-builder object.builder add --region 1 --zone 1 --ip 10.40.65.244 --port 6200 --device sdb --weight 100
sudo swift-ring-builder object.builder add --region 1 --zone 2 --ip 10.40.65.243 --port 6200 --device sdc --weight 100
sudo swift-ring-builder object.builder add --region 1 --zone 3 --ip 10.40.65.242 --port 6200 --device sdd --weight 100
swift-ring-builder object.builder rebalance

sudo swift-ring-builder account.builder
sudo swift-ring-builder container.builder
sudo swift-ring-builder object.builder

#分发ring.gz到所有节点
scp *.gz all:/etc/swift


#所有节点
chown -R root:swift /etc/swift

#控制节点
systemctl enable openstack-swift-proxy.service memcached.service
systemctl restart openstack-swift-proxy.service memcached.service

#存储节点
 systemctl enable openstack-swift-account.service openstack-swift-account-auditor.service \
  openstack-swift-account-reaper.service openstack-swift-account-replicator.service
systemctl restart openstack-swift-account.service openstack-swift-account-auditor.service \
  openstack-swift-account-reaper.service openstack-swift-account-replicator.service
systemctl enable openstack-swift-container.service \
  openstack-swift-container-auditor.service openstack-swift-container-replicator.service \
  openstack-swift-container-updater.service
 systemctl restart openstack-swift-container.service \
  openstack-swift-container-auditor.service openstack-swift-container-replicator.service \
  openstack-swift-container-updater.service
 systemctl enable openstack-swift-object.service openstack-swift-object-auditor.service \
  openstack-swift-object-replicator.service openstack-swift-object-updater.service
 systemctl restart openstack-swift-object.service openstack-swift-object-auditor.service \
  openstack-swift-object-replicator.service openstack-swift-object-updater.service