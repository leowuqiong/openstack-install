controller
openstack-config --set /etc/neutron/neutron.conf DEFAULT service_plugins router,neutron.services.qos.qos_plugin.QoSPlugin
openstack-config --set /etc/neutron/plugins/ml2/ml2_conf.ini ml2 extension_drivers port_security, qos
openstack-config --set /etc/neutron/plugins/ml2/linuxbridge_agent.ini agent extensions  qos

#ovs
#openstack-config --set /etc/neutron/plugins/ml2/openvswitch_agent.ini agent extensions  qos
compute
openstack-config --set /etc/neutron/plugins/ml2/linuxbridge_agent.ini agent extensions  qos,fip_qos

#ovs
#openstack-config --set /etc/neutron/plugins/ml2/openvswitch_agent.ini agent extensions  qos,fip_qos

#更改配置后需要重新启动neutron-linux-bridge 和openstack-nova-compute, 只重启linux bridge是不生效的
systemctl restart neutron-linuxbridge-agent.service
systemctl restart openstack-nova-compute


#创建qos限制网速, 下面操作都不需要重启
openstack network qos policy create bw-limiter
openstack network qos rule create --type bandwidth-limit --max-kbps 10240   --egress bw-limiter
openstack network qos rule create --type bandwidth-limit --max-kbps 10240   --ingress bw-limiter
#apply到端口
openstack port set --qos-policy bw-limiter     88101e57-76fa-4d12-b0e0-4fc7634b874a
#apply到浮动ip
openstack floating ip set --qos-policy bw-limiter d0ed7491-3eb7-4c4f-a0f0-df04f10a067c
#取消qos
openstack floating ip unset --qos-policy d0ed7491-3eb7-4c4f-a0f0-df04f10a067c
#apply到网络
openstack network set --qos-policy bw-limiter private
取消网络的qos
openstack network set --no-qos-policy private

openstack network qos policy  list
openstack network qos rule list bw-limiter