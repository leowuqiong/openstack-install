source openstack-rc
source variable-ha.sh
expect <<EOF
set timeout 300
  
  spawn  bash -c "openstack user create --domain default --password-prompt ceilometer"
  expect {
    "*Password*" {send "$keystone_pass\r";exp_continue}
    "*Password*" {send "$keystone_pass\r";}
   }
  
EOF

openstack role add --project service --user ceilometer admin
expect <<EOF
set timeout 300
  
  spawn  bash -c "openstack user create --domain default --password-prompt gnocchi"
  expect {
    "*Password*" {send "$keystone_pass\r";exp_continue}
    "*Password*" {send "$keystone_pass\r";}
   }
  
EOF

 mysql -uroot -p$mysql_root_pass -e " CREATE DATABASE gnocchi;"
 mysql -uroot -p$mysql_root_pass -e " GRANT ALL PRIVILEGES ON gnocchi.* TO 'gnocchi'@'localhost'   IDENTIFIED BY 'loongson';"
 mysql -uroot -p$mysql_root_pass -e " GRANT ALL PRIVILEGES ON gnocchi.* TO 'gnocchi'@'%'  IDENTIFIED BY 'loongson';"

openstack service create --name gnocchi --description "Metric Service" metric
openstack role add --project service --user gnocchi admin

openstack endpoint create --region RegionOne   metric public http://$controller:8041
openstack endpoint create --region RegionOne  metric internal http://$controller:8041
openstack endpoint create --region RegionOne   metric admin http://$controller:8041



yum install -y openstack-gnocchi-api openstack-gnocchi-metricd   python3-gnocchiclient libgfortran 

# yum install -y redis
# #修改
# #reids.conf bing=0.0.0.0
# #protected-mode no
# systemctl enable --now redis 

pip3 install uwsgi -i https://pypi.tuna.tsinghua.edu.cn/simple
  

openstack-config --set /etc/gnocchi/gnocchi.conf api auth_mode keystone
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken auth_type password
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken auth_url http://$controller:5001/v3
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken project_domain_name default
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken user_domain_name default
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken project_name service
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken username gnocchi
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken password loongson
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken interface internalURL
openstack-config --set /etc/gnocchi/gnocchi.conf keystone_authtoken region_name RegionOne
openstack-config --set /etc/gnocchi/gnocchi.conf indexer url mysql+pymysql://gnocchi:loongson@$controller/gnocchi
# openstack-config --set /etc/gnocchi/gnocchi.conf storage coordination_url redis://$controller:6379

#使用文件存储
# openstack-config --set /etc/gnocchi/gnocchi.conf storage file_basepath /var/lib/gnocchi
# openstack-config --set /etc/gnocchi/gnocchi.conf storage driver file
#i切换之后需要执行gnocchi-upgrade
# 使用ceph做存储
ceph osd pool create gnocchi
rbd pool init gnocchi
ceph auth get-or-create client.gnocchi mon 'profile rbd' osd 'profile rbd pool=gnocchi' mgr 'profile rbd pool=gnocchi'
ceph auth get-or-create client.gnocchi | ssh controller1 sudo tee /etc/ceph/ceph.client.gnocchi.keyring

openstack-config --set /etc/gnocchi/gnocchi.conf storage ceph_pool  gnocchi
openstack-config --set /etc/gnocchi/gnocchi.conf storage ceph_username  gnocchi
openstack-config --set /etc/gnocchi/gnocchi.conf storage ceph_keyring  /etc/ceph/ceph.client.gnocchi.keyring
openstack-config --set /etc/gnocchi/gnocchi.conf storage ceph_conffile  /etc/ceph/ceph.conf



chown -R gnocchi:gnocchi /var/lib/gnocchi

gnocchi-upgrade


systemctl enable openstack-gnocchi-api.service   openstack-gnocchi-metricd.service
systemctl restart openstack-gnocchi-api.service   openstack-gnocchi-metricd.service

yum install -y openstack-ceilometer-notification   openstack-ceilometer-central

vim /etc/ceilometer/polling.yaml
---
sources:
    - name: some_pollsters
      interval: 300
      meters:
        - cpu
        - cpu_l3_cache
        - memory.usage
        - network.incoming.bytes
        - network.incoming.packets
        - network.outgoing.bytes
        - network.outgoing.packets
        - disk.device.read.bytes
        - disk.device.read.requests
        - disk.device.write.bytes
        - disk.device.write.requests
        - hardware.memory.used
        - hardware.cpu.util
    - name: meter_snmp
      interval: 300
      resources:
          - snmp://10.40.73.251
          - snmp://10.40.73.252
          - snmp://10.40.73.249
      meters:
       - "hardware.cpu*"
       - "hardware.memory*"
       - "hardware.disk*"


openstack-config --set /etc/ceilometer/ceilometer.conf DEFAULT transport_url rabbit://openstack:loongson@controller1:5672,openstack:loongson@controller2:5672,openstack:loongson@controller3:5672
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials auth_type password
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials auth_url http://$controller:5001/v3
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials project_domain_id default
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials user_domain_id default
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials project_name service
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials username ceilometer
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials password loongson
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials interface internalURL
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials region_name RegionOne
ceilometer-upgrade
systemctl enable openstack-ceilometer-notification.service openstack-ceilometer-central.service
systemctl restart openstack-ceilometer-notification.service openstack-ceilometer-central.service
systemctl status openstack-ceilometer-notification.service openstack-ceilometer-central.service

##glance
openstack-config --set /etc/glance/glance-api.conf DEFAULT transport_url rabbit://openstack:loongson@10.40.73.250
openstack-config --set /etc/glance/glance-api.conf oslo_messaging_notifications driver messagingv2
openstack-config --set /etc/glance/glance-registry.conf DEFAULT transport_url rabbit://openstack:loongson@10.40.73.250
openstack-config --set /etc/glance/glance-registry.conf oslo_messaging_notifications driver messagingv2
systemctl restart openstack-glance-api.service openstack-glance-registry.service
##neutron
openstack-config --set /etc/neutron/neutron.conf oslo_messaging_notifications driver messagingv2
systemctl restart neutron-server.service

##compute node
yum -y install openstack-ceilometer-compute openstack-ceilometer-ipmi
openstack-config --set /etc/ceilometer/ceilometer.conf DEFAULT transport_url rabbit://openstack:loongson@10.40.73.250
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials auth_type password
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials auth_url http://10.40.73.250:5001/v3
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials project_domain_id default
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials user_domain_id default
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials project_name service
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials username ceilometer
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials password loongson
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials interface internalURL
openstack-config --set /etc/ceilometer/ceilometer.conf service_credentials region_name RegionOne

openstack-config --set /etc/nova/nova.conf DEFAULT instance_usage_audit True
openstack-config --set /etc/nova/nova.conf DEFAULT instance_usage_audit_period hour
openstack-config --set /etc/nova/nova.conf notifications notify_on_state_change vm_and_task_state
openstack-config --set /etc/nova/nova.conf oslo_messaging_notifications driver messagingv2

systemctl enable openstack-ceilometer-compute.service openstack-ceilometer-ipmi.service
systemctl restart openstack-ceilometer-compute.service openstack-ceilometer-ipmi.service openstack-nova-compute.service
systemctl status openstack-ceilometer-compute.service openstack-ceilometer-ipmi.service openstack-nova-compute.service


#配置snmp 收集物理主机信息
被收集的节点：
yum install -y net-snmp net-snmp-utils

修改 vim /etc/snmp/snmpd.conf
############
-access  notConfigGroup ""      any       noauth    exact  systemview none none 
+access  notConfigGroup ""      any       noauth    exact  all    none none 
-#view all    included  .1                               80 
+view all    included  .1                               80 
####################
systemctl restart snmpd


控制节点：
修改 /etc/ceilometer/polling.yaml
---
sources:
    - name: some_pollsters
      interval: 300
      meters:
        - cpu
        - cpu_l3_cache
        - memory.usage
        - network.incoming.bytes
        - network.incoming.packets
        - network.outgoing.bytes
        - network.outgoing.packets
        - disk.device.read.bytes
        - disk.device.read.requests
        - disk.device.write.bytes
        - disk.device.write.requests
        - hardware.memory.used
        - hardware.cpu.util
    - name: meter_snmp
      interval: 300
      resources:
          - snmp://10.40.73.251
          - snmp://10.40.73.252
          - snmp://10.40.73.249
      meters:
       - "hardware.cpu*"
       - "hardware.memory*"
       - "hardware.disk*
systemctl restart openstack-ceilometer-*






#  #  执行命令报错
# File "/usr/lib/python3.6/site-packages/gnocchiclient/shell.py", line 130, in build_option_parser
#     os.environ.set("OS_AUTH_TYPE", "password")
# AttributeError: '_Environ' object has no attribute 'set'
# 可以将os.environ.set("OS_AUTH_TYPE", "password")改为os.environ.setdefault("OS_AUTH_TYPE", "password")
# 或者
# echo 'export OS_AUTH_TYPE=password' >>/root/openstack-rc
# 或者使用openstack命令





