#keystone
source /root/variable-ha.sh
mysql -uroot -p$mysql_root_pass -e 'CREATE DATABASE keystone;'
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' IDENTIFIED BY '$keystone_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%' IDENTIFIED BY '$keystone_pass';"
su -s /bin/sh -c "keystone-manage db_sync" keystone

keystone-manage bootstrap --bootstrap-password $keystone_pass \
  --bootstrap-admin-url http://$vip:5001/v3/ \
  --bootstrap-internal-url http://$vip:5001/v3/ \
  --bootstrap-public-url http://$vip:5001/v3/ \
  --bootstrap-region-id RegionOne


source /root/openstack-rc
systemctl restart httpd

openstack project create --domain default   --description "Service Project" service
openstack project create --domain default   --description "Demo Project" myproject
expect <<EOF
 set timeout 300
  
  spawn  bash -c "openstack user create --domain default   --password-prompt myuser "
  expect {
    "*Password*" {send "$keystone_pass\r";exp_continue}
    "*Password*" {send "$keystone_pass\r"}
   }

EOF
openstack role create myrole
openstack role add --project myproject --user myuser myrole

#glance

mysql -uroot -p$mysql_root_pass -e "CREATE DATABASE glance;"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' IDENTIFIED BY '$glance_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' IDENTIFIED BY '$glance_pass';"

expect <<EOF 
set timeout 300 
  spawn  bash -c "openstack user create --domain default --password-prompt glance"
  expect {
    "*Password*" {send "$glance_pass\r";exp_continue}
    "*Password*" {send "$glance_pass\r";}
   } 
EOF

openstack role add --project service --user glance admin

openstack service create --name glance --description "OpenStack Image" image

openstack endpoint create --region RegionOne image public http://$vip:9293
openstack endpoint create --region RegionOne image internal http://$vip:9293
openstack endpoint create --region RegionOne image admin http://$vip:9293

openstack endpoint list

su -s /bin/sh -c "glance-manage db_sync" glance
systemctl restart openstack-glance-api.service

#nova
mysql -uroot -p$mysql_root_pass -e "CREATE DATABASE placement;"
mysql -uroot -p$mysql_root_pass -e "CREATE DATABASE nova;"
mysql -uroot -p$mysql_root_pass -e "CREATE DATABASE nova_cell0;"
mysql -uroot -p$mysql_root_pass -e "CREATE DATABASE nova_api;"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'localhost' IDENTIFIED BY '$placement_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'%' IDENTIFIED BY '$placement_pass';"



mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY '$nova_pass';"

mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY '$nova_pass';"

mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY '$nova_pass';"

expect <<EOF
  set timeout 300
  spawn  bash -c "openstack user create --domain default --password-prompt nova"
  expect {
    "*Password*" {send "$nova_pass\r";exp_continue}
    "*Password*" {send "$nova_pass\r";}
   }
  
EOF
openstack role add --project service --user nova admin
openstack service create --name nova   --description "OpenStack Compute" compute

openstack endpoint create --region RegionOne   compute public http://$vip:8784/v2.1
openstack endpoint create --region RegionOne   compute internal http://$vip:8784/v2.1
openstack endpoint create --region RegionOne   compute admin http://$vip:8784/v2.1


expect <<EOF
 set timeout 300
  
  spawn  bash -c "openstack user create --domain default --password-prompt placement"
  expect {
    "*Password*" {send "$placement_pass\r";exp_continue}
    "*Password*" {send "$placement_pass\r";}
   }
  
EOF

openstack role add --project service --user placement admin
openstack service create --name placement   --description "Placement API" placement

openstack endpoint create --region RegionOne   placement public http://$vip:8789
openstack endpoint create --region RegionOne   placement internal http://$vip:8789
openstack endpoint create --region RegionOne   placement admin http://$vip:8789
openstack endpoint list


 su -s /bin/sh -c "nova-manage api_db sync" nova
 su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
 su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
 su -s /bin/sh -c "nova-manage db sync" nova
 su -s /bin/sh -c "nova-manage cell_v2 list_cells" nova




