#!/bin/bash -e
set -x
rm -f  /etc/glance/glance-api.conf.bac
rm -f /etc/keystone/keystone.conf.bac
rm -f /etc/neutron/neutron.conf.bac
rm -f cp /etc/nova/nova.conf /etc/nova/nova.conf.bac
source variable-ha.sh
./pre_install.sh
./keystone-master.sh
source openstack-rc
./glance-master.sh
./nova-controller-master.sh
./neutron-controller-master.sh
./dashboard-master.sh

#运行cinder.sh需要先手动执行两个步骤, 详见cinder.sh
#./cinder.sh

#status check
./status-check.sh
#crontab crontab.cron

