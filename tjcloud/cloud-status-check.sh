#!/bin/bash 
export PATH=$PATH:/sbin
hostcode=0
hostname=`hostname`
echo "$hostname"
if [ $hostname = "controller1" ]
then
  hostcode=32
fi
if [ $hostname = "controller2" ]
then
  hostcode=64
fi
if [ $hostname = "controller3" ]
then
  hostcode=96
fi

echo "host code is:$hostcode"


check_galera_status(){
    CHECK_QUERY="SHOW STATUS LIKE 'wsrep_cluster_status';"

  # 连接数据库并执行查询
  CLUSTER_STATUS=$(mysql -u "root" -ploongson -e "$CHECK_QUERY"  | grep wsrep_cluster_status | awk '{print $2}')
  case "$CLUSTER_STATUS" in
        "Primary")
            return 0 ;; # 集群健康
        "Joined" | "Synced")
            return 0 ;; # 集群健康
        "Donor/Desynced")
            return 1 ;; # 集群不健康
        "Disconnected")
            return 2 ;; # 集群不健康
        "ReadOnly")
            return 2 ;; # 集群不健康
        *)
            return 3 ;; # 其他错误
    esac
}

systemctl is-active rabbitmq-server.service
res=$?
if [ $res -ne 0 ] 
  then    
    exit $((1 + hostcode))
fi
rabbitmqctl cluster_status|grep partitions|grep controller
res=$?
if [ $res -eq 0 ] 
  then
    exit $((2 + hostcode))
fi
systemctl is-active mariadb
res=$?
if [ $res -ne 0 ] 
  then
    exit $((3 + hostcode))
fi


check_galera_status
res=$?
if [ $res -ne 0 ] 
  then
    exit $((4 + hostcode))
fi
systemctl is-active keepalived
res=$?
if [ $res -ne 0 ] 
  then
    exit $((5 + hostcode))
fi
systemctl is-active haproxy
res=$?
if [ $res -ne 0 ] 
  then
    exit $((6 + hostcode))
fi
ifconfig enp0s3f1|grep -i running
res=$?
if [ $res -ne 0 ] 
  then
    exit $((7 + hostcode))
fi
systemctl is-active openstack-nova-compute
res=$?
if [ $res -ne 0 ] 
  then
    exit $((8 + hostcode))
fi
systemctl is-active  neutron-linuxbridge-agent.service 
res=$?
if [ $res -ne 0 ] 
  then
    exit $((9 + hostcode))
fi
systemctl is-active openstack-nova-compute
res=$?
if [ $res -ne 0 ] 
  then
    exit $((10 + hostcode))
fi
systemctl is-active openstack-nova-compute
res=$?
if [ $res -eq 0 ] 
  then
    exit $((11 + hostcode))
fi
systemctl is-active httpd
res=$?
if [ $res -ne 0 ] 
  then
    exit $((12 + hostcode))
fi
systemctl is-active memcached
res=$?
if [ $res -ne 0 ] 
  then
    exit $((13 + hostcode))
fi

systemctl is-active chronyd
res=$?
if [ $res -eq 0 ] 
  then
    exit  $((14 + hostcode))
fi



exit 0;

