source /root/variable-ha.sh

sed -i "/#ServerName/a\ServerName $controller" /etc/httpd/conf/httpd.conf
openstack-config --set /etc/neutron/plugins/ml2/linuxbridge_agent.ini vxlan local_ip $controller
openstack-config --set /etc/neutron/metadata_agent.ini DEFAULT nova_metadata_host $controller
openstack-config --set /etc/neutron/plugins/ml2/linuxbridge_agent.ini vxlan local_ip $controller
openstack-config --set /etc/neutron/metadata_agent.ini DEFAULT nova_metadata_host $controller
openstack-config --set /etc/nova/nova.conf DEFAULT my_ip $controller
openstack-config --set /etc/nova/nova.conf vnc novncproxy_base_url http://$controller:6090/vnc_auto.html
openstack-config --set /etc/nova/nova.conf DEFAULT my_ip $controller
openstack-config --set /etc/nova/nova.conf vnc novncproxy_base_url http://$controller:6090/vnc_auto.html
openstack-config --set /etc/cinder/cinder.conf DEFAULT my_ip $controller
openstack-config --set /etc/neutron/plugins/ml2/linuxbridge_agent.ini vxlan local_ip $compute
openstack-config --set /etc/neutron/plugins/ml2/linuxbridge_agent.ini linux_bridge physical_interface_mappings provider:$compute_interface
openstack-config --set /etc/nova/nova.conf DEFAULT my_ip $compute




mysql -uroot -ploongson -e "GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'controller1' IDENTIFIED BY 'loongson';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'controller1' IDENTIFIED BY '$glance_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'controller1' IDENTIFIED BY '$keystone_pass';"
# mysql -uroot -ploongson -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'controller1' IDENTIFIED BY 'loongson'; "
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'controller1' IDENTIFIED BY '$neutron_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'controller1' IDENTIFIED BY '$placement_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'controller1' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'controller1' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'controller1' IDENTIFIED BY '$nova_pass';"
mysql -uroot -ploongson -e "GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'controller2' IDENTIFIED BY 'loongson';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'controller2' IDENTIFIED BY '$glance_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'controller2' IDENTIFIED BY '$keystone_pass';"
# mysql -uroot -ploongson -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'controller2' IDENTIFIED BY 'loongson'; "
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'controller2' IDENTIFIED BY '$neutron_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'controller2' IDENTIFIED BY '$placement_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'controller2' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'controller2' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'controller2' IDENTIFIED BY '$nova_pass';"
mysql -uroot -ploongson -e "GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'controller3' IDENTIFIED BY 'loongson';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'controller3' IDENTIFIED BY '$glance_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'controller3' IDENTIFIED BY '$keystone_pass';"
# mysql -uroot -ploongson -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'controller3' IDENTIFIED BY 'loongson'; "
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'controller3' IDENTIFIED BY '$neutron_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'controller3' IDENTIFIED BY '$placement_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'controller3' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'controller3' IDENTIFIED BY '$nova_pass';"
mysql -uroot -p$mysql_root_pass -e "GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'controller3' IDENTIFIED BY '$nova_pass';"
